.\" Copyright 1997, 1998, 1999 Guy Maor.
.\" Adduser and this manpage are copyright 1995 by Ted Hajek,
.\" With much borrowing from the original adduser copyright 1994 by
.\" Ian Murdock.
.\" 
.\" This is free software; see the GNU General Public License version
.\" 2 or later for copying conditions.  There is NO warranty.
.TH ADDUSER 8 "" "Debian GNU/Linux"
.SH NAME
adduser, addgroup \- add or manipulate users or groups
.SH SYNOPSIS
.SY adduser
.OP [options]
.OP \-\-home dir
.OP \-\-shell shell
.OP \-\-no-create-home
.OP \-\-uid id
.OP \-\-firstuid id
.OP \-\-lastuid id
.OP \-\-firstgid id
.OP \-\-lastgid id
.OP \-\-ingroup group
.OP \-\-gid id
.OP \-\-disabled-password
.OP \-\-disabled-login
.OP \-\-gecos gecos
.OP \-\-add-extra-groups
.OP user
.SY adduser
.OP \-\-system
.OP [options]
.OP \-\-home dir
.OP \-\-shell shell
.OP \-\-no-create-home
.OP \-\-uid id
.OP \-\-group
.OP \-\-ingroup group
.OP \-\-gid id
.OP \-\-disabled-password
.OP \-\-disabled-login
.OP \-\-gecos gecos
.OP user
.SY addgroup
.OP [options]
.OP \-\-gid ID
.OP group
.SY addgroup 
.OP \-\-system
.OP [options]
.OP \-\-gid id
.OP group
.SY adduser
.OP [options]
.OP user
.OP group"
.YS
.SH DESCRIPTION
.PP
\fBadduser\fP and \fBaddgroup\fP add users and groups to the system
according to command line options and configuration information in
\fI/etc/adduser.conf\fP.
They are friendlier front ends to the low level tools like 
\fBuseradd\fP, \fBgroupadd\fP and \fBusermod\fP programs,
by default choosing Debian policy conformant UID and GID values, 
creating a home directory with skeletal configuration, running a custom 
script, and other features.
.PP
\fBadduser\fP and \fBaddgroup\fP are intended as a policy layer, making it
easier for package maintainers and local administrators to create local system
accounts in the way Debian expects them to be created, taking the burden to
adapt to the probably changing specifications of Debian policy. \fBadduser
--system\fP takes special attention on just needing a single call in the
package maintainer scripts without any conditional wrappers, error suppression
or other scaffolding.
.PP
\fBadduser\fP honors the distinction between \fIdynamically allocated system
users and groups\fP and \fIdynamically allocated user accounts\fP that is
documented in Debian Policy, Chapter 9.2.2.
.PP
\fBadduser\fP and \fBaddgroup\fP can be run in one of five modes:
.SS "Add a normal user"
If called with one non-option argument and without the
\fB\-\-system\fP or \fB\-\-group\fP  options, \fBadduser\fP
will add a normal user, that means a \fIdynamically allocated user
account\fP in the sense of Debian Policy. This is commonly referred to
in \fBadduser\fP as a \fInon-system user.\fP

\fBadduser\fP will choose the first available UID from the
range specified for normal users in the configuration file.
The UID can be overridden with the \fB\-\-uid\fP option.

The range specified in the configuration file may be overridden with the
\fB\-\-firstuid\fP and \fB\-\-lastuid\fP options.

By default, each user in Debian GNU/Linux is given a corresponding
group with the same name.
Usergroups allow group writable directories to be easily maintained
by placing the appropriate users in the new group, setting the
set-group-ID bit in the directory (which is on by default), and ensuring
that all users use a umask of 002.
If \fBUSERS_GID\fP or \fBUSERS_GROUP\fP are set, the newly
created user is placed in the referenced group as a supplemental
group. . Setting both \fBUSERS_GID\fP and \fBUSERS_GROUP\fP is an
error even if the settings are consistent.
If \fBUSERGROUPS\fP is \fIno\fP, all users get the group defined
by  \fBUSERS_GID\fP or \fBUSERS_GROUP\fP as their primary group.
Users' primary groups can also be overridden from the command
line with the \fB\-\-gid\fP  or \fB\-\-ingroup\fP options
to set the group by id or name, respectively.
Also, users can be added to one or more supplemental groups defined in
\fIadduser.conf\fP either by setting \fBADD_EXTRA_GROUPS\fP
to 1 in \fIadduser.conf\fP, or by passing \fB\-\-add\-extra\-groups\fP 
on the commandline.

\fBadduser\fP will create a home directory subject to
\fBDHOME\fP, \fBGROUPHOMES\fP, and \fBLETTERHOMES\fP.
The home directory can be overridden from the command line with the
\fB\-\-home\fP option, and the shell with the \fB\-\-shell\fP
option.
The home directory's set-group-ID bit is set if \fBUSERGROUPS\fP
is \fIyes\fP so that any files created in the user's home
directory will have the correct group.

\fBadduser\fP will copy files from \fBSKEL\fP
into the home directory and prompt for finger (GECOS) information and
a password.  The GECOS field may also be set with the \fB\-\-gecos\fP
option.
With the \fB\-\-disabled-login\fP option, the account will be created
but will be disabled until a password is set.
The \fB\-\-disabled-password\fP option will not set a password,
but login is still possible (for example with SSH keys).

If the file \fI/usr/local/sbin/adduser.local\fP exists,
it will be executed after the user account has been set
up in order to do any local setup.
.PP
\fBadduser.local\fP is also the place where local administrators
can place their code to interact with directory services, should
they desire to.
.PP
The arguments passed to \fBadduser.local\fP are:
.br
\fIusername uid gid home-directory\fP
.PP 
The environment variable \fBVERBOSE\fP is set according
to the following rule:
.TP 
0
if  \fB\-\-quiet\fP is specified
.TP 
1
if neither \fB\-\-quiet\fP nor \fB\-\-debug\fP is specified
.TP 
2
if \fB\-\-debug\fP is specified
.PP
(The same applies to the variable \fBDEBUG\fP, but
\fBDEBUG\fP is deprecated and will be removed in a later
version of \fBadduser\fP.)

.SS "Add a system user"
If called with one non-option argument and the \fB\-\-system\fP
option, \fBadduser\fP will add a \fIdynamically allocated system
user,\fP often abbreviated as \fIsystem user\fP in the context
of the \fBadduser\fP package.
If a user with the same name already exists in the system uid
range (or, if the uid is specified, if a user with that
uid already exists), \fBadduser\fP will exit with a warning.
This warning can be suppressed by adding \fB\-\-quiet\fP.

\fBadduser\fP will choose the first available UID from the range
specified for \fIsystem users\fP in the configuration file
(\fBFIRST_SYSTEM_UID\fP and \fBLAST_SYSTEM_UID\fP).
If you want to have a specific UID, you can specify it using the
\fB\-\-uid\fP option.

By default, system users are placed in the
\fBnogroup\fP group.
To place the new system user in an already existing group, use
the \fB\-\-gid\fP or \fB\-\-ingroup\fP options.
To place the new system user in a new group with the same ID, use
the \fB\-\-group\fP option.

A home directory should be specified using the \fB\%\-\-home\fP
option. If not specified, the default home directory for a new
system user is \fI\%/nonexistent\fP. This directory should never
exist on any Debian system, and \fB\%adduser\fP will not create it
automatically.

The new system user will have the shell \fI/usr/sbin/nologin\fP
(unless overridden with the \fB\-\-shell\fP option).
Standard UNIX password logins will be disabled for the new system
user; however, logins by other means (for example, via SSH) are still allowed.
Skeletal configuration files are not copied.
.SS "Add a user group"
If \fBadduser\fP is called with the \fB\-\-group\fP option and
without the \fB\-\-system\fP option, or \fBaddgroup\fP is called
respectively, a user group will be added.


A GID will be chosen from the range specified for system GIDs in the
configuration file (\fBFIRST_GID\fP, \fBLAST_GID\fP).
To override that mechanism you can give the GID using the
\fB\-\-gid\fP option.

The range specified in the configuration file may be overridden with the
\fB\-\-firstgid\fP and \fB\-\-lastgid\fP options.

The group is created with no users.
.SS "Add a system group"
If \fBaddgroup\fP is called with the \fB\-\-system\fP option,
a \fIdynamically allocated system group,\fP often abbreviated
as \fIsystem group\fP in the context of the \fBadduser\fP package,
will be created.

A GID will be chosen from the range specified for system GIDs in the
configuration file (\fBFIRST_SYSTEM_GID\fP, \fBLAST_SYSTEM_GID\fP).
To override that mechanism you can give the GID using the
\fB\-\-gid\fP option.
The system group is created with no users.

.SS "Add an existing user to an existing group"
If called with two non-option arguments, \fBadduser\fP
will add an existing user to an existing group.
.SH OPTIONS
.TP
.BR "\-c \fIfile", "\-\-conf \fIfile" 
Use \fIfile\fP instead of \fI/etc/adduser.conf\fP.
.TP
.B \-\-disabled-login
Do not run \fBpasswd\fP to set the password.
The user won't be able to use her account until the password is set.
.TP
.B \-\-disabled-password
Like \fB\-\-disabled-login\fP, but logins are still possible
(for example using SSH keys) but not using password authentication.
.TP
.B \-\-allow\-badname
By default, user and group names are checked against the configurable
regular expression \fBNAME_REGEX\fP and \fBSYS_NAME_REGEX\fP specified
in the configuration file. This option forces \fBadduser\fP and
\fBaddgroup\fP to apply only a weak check for validity of the name.
\fBNAME_REGEX\fP and \fBSYS_NAME_REGEX\fP are described in
.BR adduser.conf (5).
.TP
.B \-\-force\-badname
This is the deprecated form of \-\-allow\-badname. It will be removed
during the release cycle of the Debian release after \fIbookworm\fP.
.TP
.B \-\-allow\-all\-names
Bypass the weak name check which is used with \fB\-\-allow-badname\fP.
This will allow any username which is supported by the underlying
\fBuseradd\fP, including names containing non-ASCII characters.  The
only restrictions enforced at this level are: cannot start with a dash,
plus sign, or tilde; and cannot contain a colon, comma, slash, or whitespace.
.TP
.BI \-\-gecos " GECOS "
Set the GECOS field for the new entry generated.
\fBadduser\fP will not ask for finger information if this option is given.
.TP
.BI \-\-gid " ID "
When creating a group, this option sets the group ID number of the new
group to \fIGID\fP.  When creating a user, this option sets the primary
group ID number of the new user to \fIGID\fP.
.TP
.BI \-\-ingroup " GROUP "
When creating a user, this option sets the primary group ID number of the
new user to the GID of the named \fIGROUP\fP.  Unlike with the
\fB\-\-gid\fP option, the group is specified here by name rather than by
ID number. The group must already exist.
.TP
.B \-\-group
When combined with \fB\-\-system\fP , a group with the same name
and ID as the system user is created.
If not combined with \fB\-\-system\fP , a group with the given name
is created.
This is the default action if the program is invoked as \fBaddgroup\fP.
.TP
.BR \-h ", " \-\-help
Display brief instructions.
.TP
.BI \-\-home " dir "
Use \fIdir\fP as the user's home directory, rather than the default
specified by the configuration file.
If the directory does not exist, it is created and skeleton files are copied.
.TP
.BI \-\-shell " shell "
Use \fIshell\fP as the user's login shell, rather than the default specified
by the configuration file.
.TP
.B \-\-no-create-home
Do not create a home directory for the new user. Note that the path
name for the new user's home directory will still be entered in the
appropriate field in the \fI\%/etc/passwd\fP file. The use of this
option does not imply that this field should be empty. Rather, it
indicates to \fB\%adduser\fP that some other mechanism will be
responsible for initializing the new user's home directory if it is
to exist.
.TP
.BR \-q ", " \-\-quiet
Suppress informational messages, only show warnings and errors.
.TP
.BR  \-d ", " \-\-debug
Be verbose, most useful if you want to nail down a problem
with \fBadduser\fP.
.TP
.B \-\-system
Nomally, \fBadduser\fP creates \fIdynamically allocated user accounts and
groups\fP as defined in Debian Policy, Chapter 9.2.2. With this option,
\fBadduser\fP creates a \fIdynamically allocated system user and group.\fP
.TP
.BI \-\-uid  " ID "
Force the new userid to be the given number.
\fBadduser\fP will fail if the userid is already taken.
.TP
.BI \-\-firstuid " ID "
Override the first uid in the range that the uid is chosen from (overrides
\fBFIRST_UID\fP specified in the configuration file).
.TP
.BI \-\-lastuid " ID "
Override the last uid in the range that the uid is chosen from
(\fBLAST_UID\fP).
.TP
.BI \-\-firstgid " ID "
Override the first gid in the range that the gid is chosen from (overrides
\fBFIRST_GID\fP specified in the configuration file).
.TP
.BI \-\-lastgid " ID "
Override the last gid in the range that the gid is chosen from
(\fBLAST_GID\fP).
.TP
.B \-\-add\-extra\-groups
Add new user to extra groups defined in the configuration file. Old
spelling \-\-add_extra_groups is deprecated and will be supported in
Debian bookworm only.
.TP
.BR \-v " , " \-\-version
Display version and copyright information.

.SH EXIT VALUES

.TP
.B 0 
Success: The user or group exists as specified.
This can have 2 causes:
The user or group was created by this call to
\fBadduser\fP or the user or group was already present on the system before
\fBadduser\fP was invoked. If \fBadduser --system\fP is invoked for a
user already existing as a system user, it will also return 0.
.TP
.B 1
Creating the non-system user or group failed because it was already present.
The username or groupname was rejected because of a mismatch with the
configured regular expressions, see
.BR adduser.conf (5).
\fBadduser\fP has been aborted by a signal.
.br
Or for many other yet undocumented reasons which are printed
to console then.
You may then consider to remove \fB\-\-quiet\fP to make
\fBadduser\fP more verbose.

.SH SECURITY
\fBadduser\fP needs root privileges and offers, via the \fB\-\-conf\fP
command line option to use a different configuration file. Do not use
\fBsudo\fP or similar tools to give partial privileges to \fBadduser\fP
with restricted command line parameters. This is easy to circumvent and might
allow users to create arbitrary accounts. If you want this, consider writing
your own wrapper script and giving privileges to execute that script.

.SH FILES
.TP 
.I /etc/adduser.conf
Default configuration file for \fBadduser\fP and \fBaddgroup\fP
.TP
.I /usr/local/sbin/adduser.local
Optional custom add-ons.

.SH NOTES
Unfortunately, the term \fIsystem account\fP suffers from double use in Debian.
It both means an account for the actual Debian system, distinguishing itself
from an \fIapplication account\fP which might exist in the user database of
some application running on Debian. A \fIsystem account\fP in this definition
has the potential to log in to the actual system, has a UID, can be member in
system groups, can own files and processes. Debian Policy, au contraire, in its
Chapter 9.2.2, makes a distinguishment of \fIdynamically allocated system users
and groups\fP and \fIdynamically allocated user accounts\fP, meaning in both
cases special instances of \fIsystem accounts\fP. Care must be taken to not
confuse this terminology. Since \fBadduser\fP and \fBdeluser\fP never address
\fIapplication accounts\fP and everything in this package concerns \fIsystem
accounts\fP here, the usage of the terms \fIuser account\fP and \fIsystem
account\fP is actually not ambiguous in the context of this package. For clarity,
this document uses the definition \fIlocal system account or group\fP if
the distinction to \fIapplication accounts\fP or accounts managed in a directory
service is needed.
.PP
\fBadduser\fP used to have the vision to be the universal front end to the
various directory services for creation and deletion of regular and system
accounts in Debian since the 1990ies. This vision has been abandoned as of
2022. The rationale behind this includes: that in practice, a small
server system is not going to have write access to an enterprise-wide directory
service anyway, that locally installed packages are hard to manage with
centrally controlled system accounts, that enterprise directory services have
their own management processes anyway and that the personpower of the
\fBadduser\fP is unlikely to be ever strong enough to write or support the
plethora of directory services that need support.
.PP
\fBadduser\fP will constrict itself to being a policy layer for the management
of local system accounts, using the tools from the \fBpassword\fP package for
the actual work.

.SH BUGS
Inconsistent use of terminology around the term \fIsystem account\fP in docs
and code is a bug. Please report this and allow us to improve our docs.
.PP
\fBadduser\fP takes special attention to be directly usable in Debian
maintainer scripts without conditional wrappers, error suppression and other
scaffolding. The only thing that the package maintainer should need to code is a
check for the presence of the executable in the postrm script. The
\fBadduser\fP maintainers consider the need for additional scaffolding a
bug and encourage their fellow Debian package maintainers to file bugs against
the \fBadduser\fP package in this case.

.SH SEE ALSO
.BR adduser.conf (5),
.BR deluser (8),
.BR groupadd (8),
.BR useradd (8),
.BR usermod (8),
Debian Policy 9.2.2.

.SH COPYRIGHT
Copyright (C) 1997, 1998, 1999 Guy Maor. Modifications by Roland
Bauerschmidt and Marc Haber. Additional patches by Joerg Hoh and Stephen Gran.
.br
Copyright (C) 1995 Ted Hajek, with a great deal borrowed from the original
Debian  \fBadduser\fP
.br
Copyright (C) 1994 Ian Murdock.
\fBadduser\fP is free software; see the GNU General Public Licence
version 2 or later for copying conditions.
There is \fIno\fP warranty.
